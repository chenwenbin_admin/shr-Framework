# SHR (shooting range) 靶场 
> 看到框架名字时应该要配一个滑稽表情

## 叙述
***

> 这框架是业余的一个小项目,目的是为了学习与巩固自己所学所用,同时写文章时也可以锻炼自己的文字表达能力以及__耐心__


> 框架中借鉴了许多其他框架的使用方法,在平时工作中呢,每学到一个新知识或者想出可以写更好的地方,我就会往上添砖加瓦翻新改造,这也是我给这框架命名为靶场的原因,在此共勉! 加油! ~ -_-

***

##### 记录版本迭代时间
项目开始时间: 2016年09月13日

1. 0.0.1版 2016年09月16日 说明: 完成的最基本,路由,CURD,MVC功能,以及一些常用的类库
***

#### 框架说明

> 框架只支持路由,也就是说,必须要重写url规则,隐藏入口文件
> 路由规则只支持http:host.com/controller/action/a/1/b/2/c3这种类型的路由
> 目前框架的类库都只实现了最基本的功能



***

### 目前框架实现的功能
***
1. **基础路由**
2. **MVC**
3. **类库**  [未使用任何第三方类库]
4. **使用composer自带的自动加载类**  [注: 因为用起来很爽,但没有安装任何composer中的第三方库]
5. **支持插件机制**

***

### 基本类库描述
***
这些文件都位于lib文件夹下

> 1. request.php [接收或处理http参数请求的类]
> 2. upload.php [文件上传类]
> 3. log.php  [日志类]
> 4. model.php ^ db_mysqli.php MODEL基础类 [数据库操作] 
> 5. session.php [session操作类]
> 6. validate.php [数据验证类, 一般用于表单验证]
> 7. view.php & view_base.php VIEW基础类
> 8. routes.php [路由类] 主要用于解析URL分发到指定的控制器与方法
> 9. plugin.php [插件核心类]

# 快速入门


## 使用准备


1. 安装composer管理工具, 框架的自动加载类就是基于它
2. 进入源代码的根目录运行命令 `composer update`
3. 将public目录映射成网站根目录, 并重写URL规则,隐藏index.php入口文件




## 实例一 Hello World

1. 使用php自带的webserver工具, 隐藏public文件夹下的index.php入口文件

	---
		cd public
		php -S localhost:8888 index.php
2. 设置路由


	打开config/app/routes.php文件 写入下面代码
	
	---
		//作用:当访问网站根目录时,默认访问HomeController控制器下的 index 方法
		return [
	    	'/' => 'HomeController@index',
		];
	
	

3. 创建控制器文件与视图文件

	在app/controllers/下创建  home.php
	
	在app/views/home/下创建   index.php
	
4. 编写代码
	控制器文件 app/controllers/home.php	
	--- 
		
		<?php
		class HomeController {
		    public function index(Request $req, Response $res) {
		        //视图调用 示例
		        $res->view('home/index') //加载views/home/index.php视图文件
		        ->make('hello','Hello world') //给视图赋予变量$hello 值为Hello world
		        ->display(); //显示视图
		    }
		}
		
	视图文件 app/views/home/index.php
	---
		视图文件 app/views/home/index.php
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
			<title><?php echo $hello;?></title>
		</head>
		<body>
			<?php echo $hello;?>
		</body>
		</html>
		
	
5. 运行命令将新建的控制器添加进自动加载类的名单
	切换进 项目根目录 运行以下命令
	---
		composer dump-autoload
	
		
6. 大功告成,在浏览器输入 localhost:8888 即可看到效果

***

## 实例二 表单提交接收与验证(待续)
## 实例三 session操作(待续)
## 实例四 操作数据库(待续)




