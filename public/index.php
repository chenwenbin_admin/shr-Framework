<?php
define('BASE_PATH',dirname(__DIR__)); //全局根目录
define('VIEW', BASE_PATH . '/app/views/'); //设置视图根目录
define('CONFIG_PATH',BASE_PATH . '/config/app'); //设置配置文件根目录

ini_set('display_errors', 1);
error_reporting( E_ALL );
#自动加载类
require "../vendor/autoload.php";


Model::loadConfig(require CONFIG_PATH . '/database.php');#加载数据库配置文件
View::setViewDir(VIEW);#设置视图根目录

Routes::load(require CONFIG_PATH . '/routes.php');#加载路由配置
Routes::flush();#启动路由,接收请求


