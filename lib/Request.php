<?php

class Request {
	public $value;

	/**
	 * 获取表单值
	 * @param  [type] $name    [description]
	 * @param  string $default [description]
	 * @return [type]          [description]
	 */
	public function input($name, $default = '') {
		if( isset($_REQUEST[$name]) && is_object($default) ) {
			$value = addslashes($_REQUEST[$name]);
			$default(new Validate($value));
			return $value;
		}

		if ( !isset($_REQUEST[$name]) ) {
			return $default;
		}

		if ( is_numeric($_REQUEST[$name]) ) {
			return $_REQUEST[$name];
		}

		return empty($_REQUEST[$name]) ? $default : addslashes($_REQUEST[$name]);
	}
	/**
	 * 获取整个表单值
	 * @return [type] [description]
	 */
	public function form( Array $allow = array() ) {
		foreach ($_REQUEST as $key => &$value) {
			if (!empty($allow) && !in_array($key, $allow) ) continue;
			$value = addslashes($value);
		}
		return $_REQUEST;
	}

	/**
	 * 是否为post提交
	 * @return boolean [description]
	 */
	public function isPost() {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
	}
	/**
	 * 是否为get提交
	 * @return boolean [description]
	 */
	public function isGet() {
        return !empty($_GET);
	}
	/**
	 * 是否为post提交
	 * @return boolean [description]
	 */
	public function isAjax() {
        return isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"])=="xmlhttprequest";
	}
	/**
	 * 获取客户端IP
	 * @return [type] [description]
	 */
	public function clientIP() {
    	return $_SERVER["REMOTE_ADDR"];
    }

    
}