<?php
/**
* \View
*/
class View {
    public $file;
    public $data = [];
    /**
     * 引入模板视图
     * @param  [type] $file [description]
     * @return [type]           [description]
     */
    public function load($file) {
        if ( in_array($file[0], ['/','.']) ) {
            $file = $file . '.php';
        } else {
            $file = PROJECT . '/views/' . $file . '.php';
        }
        if( !is_file($file) ) {
            trigger_error("Not Found File: " .$file);die;
        }
        $this->file = $file;
        return $this;
    }
    
    /**
     * 导入模板变量
     * @param  [type] $key   导入的变量名
     * @param  [type] $value 对应变量名的值
     * @return [type]        [description]
     */
    public function make($key, $value=NULL) {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * 显示模板
     * @return [type] [description]
     */
    public function display() {
        extract($this->data);
        require $this->file;
    }
}
