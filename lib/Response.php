<?php

class Response {
	public $view; #存放视图对象

	public function __construct() {
		$this->view = new View();
	}

	/**
	 * 使用view对象
	 * @param  [type] $file [description]
	 * @return [type]       [description]
	 */
	public function view($file) {
		return $this->view->load($file);
	}

	/**
	 * 显示404页面
	 * @return [type] [description]
	 */
	public function errorPage() {
		$this->view('./404page')->display();
	}
	
	/**
	 * 向页面输出json数据
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function outputJson($data) {
		echo json_encode($data);exit;
	}


}