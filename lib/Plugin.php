<?php

/**
 * 插件核心类[观察者模式]
 */
class Plugin {
	//监听的事件
	private $_listeners = [];

	public function __construct() {
		
		if ( empty(self::$config) ) {
			trigger_error('请先加载插件的配置文件');die;
		}

		$config = Common::config('app/plugin');
		//加载插件
		foreach ($config as $plugin) {
			include_once BASE_PATH . '/'. $plugin['dir'] . '/' . $plugin['name'] . '.php';
			//注册插件
			new $plugin['name']($this);
		}

	}

	/**
	 * 注册需要监听的插件方法
	 * @param  [type] $hook   [钩子名称]
	 * @param  [type] &$obj   [插件对象]
	 * @param  [type] $method [插件要调用的方法]
	 * @return [type]         [description]
	 */
	public function register( $hook, &$obj, $method) {
		$key = get_class($obj).'->'.$method;
		$this->_listeners[$hook][$key] = [&$obj, $method];
	}

	/**
	 * 触发一个钩子
	 * @param  [type] $hook [description]
	 * @param  string $data [description]
	 * @return [type]       [description]
	 */
	public function trigger( $hook, $data = '') {
		$res = '';
		if ( empty($this->_listeners[$hook]) ) {
			return $res;
		}

		/**
		 * 遍历调用钩子方法
		 */
		foreach ($this->_listeners[$hook] as  $listener) {
			$obj =& $listener[0];
			$method = $listener[1];

			if (method_exists($obj, $method)) {
				$res .= $obj->$method($data);
			}
		}
		return $res;
	}


}