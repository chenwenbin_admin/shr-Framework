<?php
/**
 * 公共工具类
 */
class Common {

	/**
     * 跳转
     * @param  [type] $uri [description]
     * @param  string $dns 域名,默认为当前域名
     * @return [type]      [description]
     */
    static public function redirect($uri, $dns = '') {
        header('Location:' . $dns . $uri);
    }

    /**
     * 获取配置文件
     */
	static public function config($path, $key='') {
		$file = CONFIG_PATH . '/' . $path . '.php';	
		if (!is_file($file)) {
			trigger_error('失败: 获取'.$file.'配置文件失败: ');
			return false;
		}
		$config = require $file;
		if ($key) {
			return $config[$key];
		}
		return $config;
	}
}