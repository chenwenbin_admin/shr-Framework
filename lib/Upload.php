<?php
/**
 * 文件上传路径
 */
class Upload {
	#文件保存路径
	public $savePath;
	#文件
	private $files = array();
	#允许上传的文件类型
	private $exts = array();
	#允许上传的文件大小 M
	private $size;  

	#重命名的名字长度
	private $nameLen = 20;

	/**
	 * @param [type] $file [文件]
	 */
	public function __construct($file, $savePath, $len = 20) {
		$this->savePath = $savePath . '/';
		$this->files = $file;
		$this->nameLen = $len;
		@$this->mkDirs($this->savePath);
	}
	/**
	 * 允许上传的文件类型
	 * @return [type] [description]
	 */
	public function allowExt($exts) {
		$this->exts = $exts;
		return $this;
	}
	/**
	 * 允许上传的文件大小
	 * @return [type] [description]
	 */
	public function allowSize($MB) {
		$this->size = $MB * 1024 * 1024;
		return $this;
	}

	/**
	 * 开始上传
	 * @return [type] [description]
	 */
	public function finish() {
	   	
	   	/**
	   	 * 多文件上传
	   	 */
	   	if ( is_array($this->files['name']) ){
	   		return $this->uploadAll();
	   	}

	   	/**
	   	 * 单文件上传
	   	 * @var [type]
	   	 */
	   	$file = $this->files;//得到传输的数据
		//得到文件名称
		$name = $file['name'];
		$type = strtolower(substr($name,strrpos($name,'.')+1)); //得到文件类型，并且都转化成小写
		//判断文件类型是否被允许上传
		if(!empty($this->exts) && !in_array($type, $this->exts)){
		  	return -1;
		}
		//判断是否是通过HTTP POST上传的
		if(!is_uploaded_file($this->files['tmp_name'])){
		  //如果不是通过HTTP POST上传的
		  	return -2;
		}
		//判断是否是文件大小是否超出文件大小
		if( $this->size && ($this->size > $this->files['size']) ){
		  //如果不是通过HTTP POST上传的
		  	return -3;
		}
		$uploadfile = $this->savePath . $this->randName() . '.' . $type;
	    $errorFiles = [];
    	//开始移动文件到相应的文件夹
		if(!move_uploaded_file($this->files['tmp_name'],$uploadfile)){
		  	$errorFiles[] = $upload_path.$file['name'];
		} else {
			@chmod($uploadfile, 0777);
		}
		if( !empty($errorFiles) ) {
	    	return $errcode;
		}
		return true;
	}

	public function uploadAll() {
		$count = count($this->files['name']);
	    for ($i = 0; $i < $count; $i++) {
	    	$file = $this->files;//得到传输的数据
			//得到文件名称
			$name = $file['name'][$i];
			$type = strtolower(substr($name,strrpos($name,'.')+1)); //得到文件类型，并且都转化成小写
			//判断文件类型是否被允许上传
			if(!empty($this->exts) && !in_array($type, $this->exts)){
			  	return -1;
			}
			//判断是否是通过HTTP POST上传的
			if(!is_uploaded_file($this->files['tmp_name'][$i])){
			  //如果不是通过HTTP POST上传的
			  	return -2;
			}
			//判断是否是文件大小是否超出文件大小
			if( $this->size && ($this->size > $this->files['size'][$i]) ){
			  //如果不是通过HTTP POST上传的
			  	return -3;
			}
			
	    }
	    $uploadfile = $this->savePath . $this->randName() . '.' . $type;
	    $errorFiles = [];
	    for ($i = 0; $i < $count; $i++) {
	    	//开始移动文件到相应的文件夹
			if(!move_uploaded_file($this->files['tmp_name'][$i],$uploadfile)){
			  	$errorFiles[] = $upload_path.$file['name'][$i];
			} else {
				@chmod($uploadfile,0777);
			}
	    }
		if( !empty($errorFiles) ) {
	    	return $errcode;
		}
		return true;
	}

	/**
	 * 文件重命名[随机打乱]
	 */
	private function randName() {
		$str = "qwertyuiopasdfghjklzxcvbnm1234567890";
		$name = '';
		for ($i = 0; $i < $this->nameLen; $i++) {
			$name .= $str[rand(0,strlen($str) - 1)];
		}
		return $name;
	}


	public function mkDirs($dir){
	    if(!is_dir($dir)){
	        if(!$this->mkDirs(dirname($dir))){
	            return false;
	        }
	        if(!mkdir($dir,0777)){
	            return false;
	        }
	        if(!chmod($dir,0777)) {
	            return false;
	        }
	    }
	    return true;
	}

}