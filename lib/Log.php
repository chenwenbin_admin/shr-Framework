<?php
/**
 * log 文件系统
 */

class Log {


	/**
	 * 写日志
	 * @param $content 要写入的内容
	 * @param $dir 写入到哪个目录
	 * @param $uniqid 唯一标识符,一般用于针对性的调试,定位.
	 */
	static public function write( $content, $uniqid  = '无',$dir = './logs/apperror/' ) {
		if (is_array($content)) {
			$content = json_decode($content,true);
		}
		$conent = "时间:*". date('H:i:s') . "* [{$uniqid}] # $content #\n";
		//创建文件夹
		if ( !is_dir($dir) ) {
			@self::mkDirs($dir);
		}
		//写入日志
		file_put_contents($dir . date('Y-m-d') . '.txt', $conent, FILE_APPEND);
		@chmod($dir . date('Y-m-d') . '.txt', 0777);
		return true;
	}


	/**
	 * 捕获php错误,并写入日志
	 * @return [type] [description]
	 */
	static public function getHandlerError() {
		register_shutdown_function(function() {
			$e = error_get_last();
			if (!empty($e)) {
				$errmsg = "{$e['message']} {$e['file']} in line: {$e['line']}";
				$response = new Response();
				$exit = FALSE;
				switch ($e['type']) {
					case '1':
				 		Log::write( $errmsg, '致命错误');
				 		$response->errorPage();
						break;
					case '4':
				 		Log::write( $errmsg, '语法错误');
				 		$response->errorPage();
						break;
				 	case '8':
				 		Log::write( $errmsg, '普通错误');
				 		break;
				 	default: 
				 		Log::write( $errmsg, '致命错误');
				 		$response->errorPage();
				 		break;
				}

			}
		});
	}

	
	static public function mkDirs($dir){
	    if(!is_dir($dir)){
	        if(!self::mkDirs(dirname($dir))){
	            return false;
	        }

	        if(!mkdir($dir,0777)){
	            return false;
	        }
	        if(!chmod($dir,0777)) {
	            return false;
	        }
	    }
	    return true;
	}
}