<?php
class Model extends DB_Mysqli{

    static $link = NULL;
    public function __construct($table='') {
        $config = Common::config('app/database');

        if ( $table ) {
            $this->table = $table;
        }

        if ( !is_object(self::$link) ) {
            self::$link = $this->connet($config);
        }

        $this->setTable($config['prefix'] . $this->table);

    }
   
    /**
     * 创建简易model
     * @param  [type] $table [description]
     * @return [type]        [description]
     */
    static public function table($table) {
        return new self($table);
    }

}
