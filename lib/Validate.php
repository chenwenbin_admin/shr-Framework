<?php
/**
 * 数据验证类
 */
class Validate {
	private $value;

	public function __construct($value) {
		$this->value = $value;
	}

	public function val() {
		return $this->value;
	}
	
	/**
	 * 是否为空
	 * @return boolean [description]
	 */
	public function isEmpty() {
		return empty($this->value);
	}
	/**
	 * 是否为数字
	 * @return boolean [description]
	 */
	public function isNumber() {
		return is_numeric($this->value);
	}
	/**
	 * 是否为纯汉字
	 * @return boolean [description]
	 */
	public function isChina($charset = 'utf8') {
		return preg_match("/^[\x{4e00}-\x{9fa5}]+$/u",$this->value) ? true : false;
	}
	/**
	 * 是否为邮箱
	 * @return boolean [description]
	 */
	public function isEmail() {
		return preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/",$this->value) ? true : false;
	}
	/**
	 * 是否为手机号
	 * @return boolean [description]
	 */
	public function isPhone() {
		return preg_match("/^(0|86|17951)?(13[0-9]|15[012356789]|1[78][0-9]|14[57])[0-9]{8}$/",$this->value) ? true : false;
	}
	/**
	 * 是否为年份 格式：yyyy
	 * @return boolean [description]
	 */
	public function isYear() {
		return preg_match("/^(\d{4})$/",$this->value) ? true : false;
	}
	/**
	 * 是否为日期时间 格式：yyyy-mm-dd hh:ii:ss
	 * @return boolean [description]
	 */
	public function isDateTime() {
		return preg_match("/^(\d{4})-(0?\d{1}|1[0-2])-(0?\d{1}|[12]\d{1}|3[01])\s(0\d{1}|1\d{1}|2[0-3]):[0-5]\d{1}:([0-5]\d{1})$/",$this->value) ? true : false;
	}
	/**
	 * 是否为网址 
	 * @return boolean [description]
	 */
	public function isUrl() {
		return preg_match("/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/",$this->value) ? true : false;
	}
	/**
	 * 是否为身份证
	 * @return boolean [description]
	 */
	public function isIdCard() {
		return preg_match("/(^\d{15}$)|(^\d{17}([0-9]|X)$)/",$this->value) ? true : false;
	}
	/**
	 * 是否为邮编
	 * @return boolean [description]
	 */
	public function emailCode() {
		return preg_match("/[1-9]\d{5}(?!\d)/",$this->value) ? true : false;
	}

	/**
	 * 是否为IPv4
	 * @return boolean [description]
	 */
	public function isIP4() {
		return preg_match("/^(((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))\.){3}((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))$/",$this->value) ? true : false;
	}
	/**
	 * 是否为IPv6
	 * @return boolean [description]
	 */
	public function isIP6() {
		return preg_match("/^([\da-fA-F]{1,4}:){7}[\da-fA-F]{1,4}$/",$this->value) ? true : false;
	}

}