<?php
/**
 * session基本操作类
 */
class Session {

    private static $_start = FALSE;
    //session前缀
    private static $prefix = '_';

    static public function instanceStart() {
        if( defined('SESSION_PREFIX') ) {
            self::$prefix = SESSION_PREFIX;
        }
        if (self::$_start === FALSE) {
            @session_start();
            self::$_start = TRUE;
        }
        return true;
    }

    /**
     * 写入session
     * @param  [type] $key   [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    static public function put($key, $value) {
        self::instanceStart();
        if (!$key) return false;
        if ( strpos($key,':') === false ) {
            $_SESSION[self::$prefix . $key] = $value;
            return;
        }

        $keys = explode(':',$key);
        $eval = '';
        foreach ($keys as $index => $v) {
            if (!$v) return false;
            if( $index == 0) {
                $eval .= "['".self::$prefix."$v']";
            } else {
                $eval .= "['$v']";
            }
        }
        $eval = '$_SESSION' . $eval . '=$value;';
        eval($eval);
        return true;
    }


    /**
     * [获取session值]
     * @param  [type] $key     [description]
     * @param  string $defalut [如果$key不存在,则返回此值]
     * @return [type]          [description]
     */
    static public function get( $key, $defalut='') {
        self::instanceStart();
        if (!$key) return false;

        if ( strpos($key,':') === false ) {
            if (isset($_SESSION[self::$prefix . $key])) {
                return $_SESSION[self::$prefix . $key];
            } else {
                return $defalut;
            }
        }
        $keys = explode(':',$key);
        $eval = '';
        foreach ($keys as $index => $v) {
            if( $index == 0) {
                $eval .= "['".self::$prefix."$v']";
            } else {
                $eval .= "['$v']";
            }
        }
        $eval = 'return @$_SESSION' . $eval . ' ? @$_SESSION' . $eval . ' : $defalut;';
        return eval($eval);
    }

    /**
     * 删除指定的Session
     * @param  [type] $key [删除指定key的session
     * @return [type]      [description]
     */
    static public function remove( $key='' ) {
        self::instanceStart();
        if (!$key) { //清除所有匹配到前缀的session
            $len = strlen(self::$prefix);
            foreach ($_SESSION as $key => $value) {
                if ( substr($key,0,$len) == self::$prefix ) {
                    unset($_SESSION[$key]);
                }
            }
            return ;
        }

        if ( strpos($key,':') === false ) {
            if (isset($_SESSION[self::$prefix . $key])) {
                unset($_SESSION[self::$prefix . $key]);
                return true;
            } else {
                return false;
            }
        }


        $keys = explode(':',$key);
        $eval = '';
        foreach ($keys as $index => $v) {
            if( $index == 0) {
                $eval .= "['".self::$prefix."$v']";
            } else {
                $eval .= "['$v']";
            }
        }
        $eval = '@$_SESSION' . $eval . ' = "";';
        return eval($eval);
    }


    

}
