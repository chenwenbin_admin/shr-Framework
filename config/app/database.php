<?php

return [

    'driver'    => 'Mysqli', #设置底层mysql操作库

    'host'      => 'localhost',

    'database'  => 'myhome', //数据库名称

    'user'  => 'root', //账号

    'pass'  => '123456', //密码

    'charset'   => 'utf8', //使用的字符集

    'prefix'    => '', //表前缀

    'port' => 3306,

    'debug' => true,

];
