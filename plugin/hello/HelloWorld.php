<?php

class HelloWorld {

	public function __construct(Plugin &$plugin) {

		//监听插件
		$plugin->register( 'demo', $this, 'sayHello' );
	}

	public function sayHello () {
		echo 'HelloWorld';
	}

}