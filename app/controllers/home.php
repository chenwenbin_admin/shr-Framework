<?php
class HomeController {
    public function index(Request $req, Response $res) {
        //视图调用 示例
        $res->view('home/index') //加载views/home/index.php视图文件
        ->make('hello','Hello world') //给视图赋予变量$hello 值为Hello world
        ->display(); //显示视图
    }
}
